package com.cybozu.labs.langdetect;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

/**
 * {@link Language} is to store the detected language.
 * {@link Detector#getProbabilities()} returns an {@link ArrayList} of {@link Language}s.
 *  
 * @see Detector#getProbabilities()
 * @author Nakatani Shuyo
 *
 */
public class Language {
	public static String[] supportedlangs;
    public String lang;
    public double prob;
    
    private Properties prop = new Properties();
    private InputStream input = null;
    
    public static final String SUPPORTED_LANGUAGE = "LANGUAGE_DETECT_LANGUAGE_SUPPORTED";
    private static String SUPPORTED_LANGUAGE_VALUE="";
    static{
    	
    }
    public Language(String lang, double prob) {
    	input = this.getClass().getClassLoader().getResourceAsStream("serverconfig.properties");
		if (input != null) {
			try {
				prop = new Properties();
				prop.load(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		if(SUPPORTED_LANGUAGE_VALUE.equals(""))
		SUPPORTED_LANGUAGE_VALUE=prop.getProperty(SUPPORTED_LANGUAGE);
		
    	
    	this.lang = lang;
        this.prob = prob;
        
        
        
    }
    public String toString() {
        if (lang==null) return "";
        return lang + ":" + prob;
    }
    public static void main(String[] args)
    {
    	
    }
}
