package cdac.nvli.util.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import org.apache.mahout.classifier.naivebayes.BayesUtils;
import org.apache.mahout.classifier.naivebayes.NaiveBayesModel;
import org.apache.mahout.classifier.naivebayes.StandardNaiveBayesClassifier;
import org.apache.mahout.common.Pair;
import org.apache.mahout.common.iterator.sequencefile.SequenceFileIterable;
import org.apache.mahout.math.RandomAccessSparseVector;
import org.apache.mahout.math.Vector;
import org.apache.mahout.math.Vector.Element;
import org.apache.mahout.vectorizer.TFIDF;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.Multiset;

import cdac.nvli.bean.TranslationBean;
import cdac.nvli.bean.TranslationObjectsFromDB;
import cdac.nvli.bean.Word;
import cdac.nvli.util.service.QueryTranslationService;

/**
 * @author Lovey@cdacpune
 * @since build 1.1
 */

public class ProcessingDao {

	// varriables
	private Properties prop = new Properties();
	private InputStream input = null;
	public static Logger logger = Logger.getLogger("SearchResources");
	private static String MAHOUT_DOMAIN_DATA_PATH;
	private static LinkedHashSet<String> languageSupported;
	private static Map<String, TranslationObjectsFromDB> mapOfTranslationObjects;
	private static Map<String, LinkedHashSet<String>> mapOfStopwords;
	private static QueryTranslationService queryTranslationService = null;
	private static Map<String, String> languageCodesLANGtoLA = null;
	private static Map<String, String> languageCodesLAtoLANG = null;
	private static String NE_DICTIONARY_LOCATION = "NE_DICTIONARY_LOCATION";
	private static String TRANSLATION_DICTIONARY_LOCATION = "TRANSLATION_DICTIONARY_LOCATION";
	private static String TRANSLITERATION_DICTIONARY_LOCATION = "TRANSLITERATION_DICTIONARY_LOCATION";
	private static String WORDNET_LOCATION = "WORDNET_LOCATION";
	private static String EXTERNAL_SERVICE_FOR_TRANSLITERATION = "EXTERNAL_SERVICE_FOR_TRANSLITERATION";
	private static String EXTERNAL_SERVICE_URL;
	private static Detector detector = null;
	private ArrayList<Word> debugWordList = new ArrayList<>();
	private HttpClient httpClient = new DefaultHttpClient();
	private static String EXTERNAL_API_HOSTNAME;
	private static String EXTERNAL_API_PORT;

	/**
	 * ProcessingDao implementation for general data-access object utilizations of
	 * Crosslingual classes
	 * 
	 * @return void
	 * @author Lovey Joshi
	 * 
	 */
	/**
	 * Default Constructor Setup of LanguageCodes.properties serverconfig.properties
	 */
	public ProcessingDao() {
		logger.log(Level.INFO, "ProcessingDao Initialized");

		// Get the language codes form LanguageCodes.txt for their short
		// names-long names format : en--English for wordnet
		input = this.getClass().getClassLoader().getResourceAsStream("LanguageCodes.properties");
		if (input != null) {
			try {
				prop = new Properties();
				prop.load(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		if (languageCodesLANGtoLA == null || languageCodesLANGtoLA.isEmpty()) {
			languageCodesLANGtoLA = new HashMap<>();
			languageCodesLAtoLANG = new HashMap<>();
			for (Object key : prop.keySet()) {
				languageCodesLANGtoLA.put(key.toString(), (String) prop.get(key));
				languageCodesLAtoLANG.put((String) prop.get(key), key.toString());
			}
		}
		prop.clear();
		// logger.log(Level.INFO, "LannguageCodes: " + languageCodes +
		// "\nLanguageSize:" + languageCodes.size());

		// Get the language codes from serverconfig, that are supported in this
		// version
		input = this.getClass().getClassLoader().getResourceAsStream("serverconfig.properties");
		if (input != null) {
			try {
				prop = new Properties();
				prop.load(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + input + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		if (languageSupported == null || languageSupported.isEmpty()) {
			String[] spiltLanguages = prop.get("LANGUAGES_SUPPORTED").toString().split(",");
			languageSupported = new LinkedHashSet<>();
			languageSupported = new LinkedHashSet<String>(Arrays.asList(spiltLanguages));
		}
		if (MAHOUT_DOMAIN_DATA_PATH == null || MAHOUT_DOMAIN_DATA_PATH.isEmpty())
			MAHOUT_DOMAIN_DATA_PATH = prop.getProperty("MAHOUT_DIR");
		logger.log(Level.INFO, "Lannguage supported: " + languageSupported);

		/*
		 * Query Translation Service instance for transliteration model lookup
		 */
		queryTranslationService = QueryTranslationService.getInstance();
		/*
		 * Create map of Transliteration objects from dictionary lookup For NE lookup,
		 * dictionary name will be <lang1>TO<lang2>NEDICT For TRANSLATION lookup,
		 * dictionary name will be <lang1>TO<lang2>TRANSLATEDICT For TRANSLITERATION
		 * lookup, dictionary name will be <lang1>TO<lang2>TRANSLITEDICT For WORDNET
		 * lookup, dictionary name will be <lang1>TO<lang2>WORDNET
		 * 
		 */
		mapOfTranslationObjects = new HashMap<>();
		mapOfTranslationObjects = createDictionaries();
		// logger.log(Level.INFO,
		// "mapOfTranslationObjects:"+mapOfTranslationObjects);
		/* Create Stopwords Map from property */
		mapOfStopwords = new HashMap<>();
		mapOfStopwords = createStopwordsMap();

		try {
			DetectorFactory.loadProfile(new File(prop.get("LANGUAGE_DETECT_PROFILE_DIRECTORY").toString()));

		} catch (LangDetectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		EXTERNAL_SERVICE_URL = prop.getProperty(EXTERNAL_SERVICE_FOR_TRANSLITERATION);
		EXTERNAL_API_HOSTNAME = prop.getProperty("EXTERNAL_API_HOSTNAME");
		EXTERNAL_API_PORT = prop.getProperty("EXTERNAL_API_PORT");
	}

	private Map<String, LinkedHashSet<String>> createStopwordsMap() {

		Map<String, LinkedHashSet<String>> mapOfStopword = new HashMap<>();
		for (String lang : languageSupported) {
			String language = languageCodesLAtoLANG.get(lang);
			logger.log(Level.INFO, "Creating Stopword Map for " + lang + ":" + language);
			try {
				BufferedReader bfr = new BufferedReader(new FileReader(
						new File(prop.getProperty("STOPWORDS_DIRECTORY") + "/stopwords_" + lang + ".txt")));
				String readline = "";
				try {
					LinkedHashSet<String> elements = new LinkedHashSet<>();
					while ((readline = bfr.readLine()) != null) {
						elements.add(readline.trim());
					}
					mapOfStopword.put(lang, elements);
				} catch (IOException e) {

					logger.warning(e.getMessage());
				}
			} catch (FileNotFoundException e) {

				logger.warning("File not Found for " + lang + ":" + language);
			}
		}
		return mapOfStopword;
	}

	/**
	 * Creating dictionaries
	 * 
	 * @return Map<String, TranslationObjectsFromDB>
	 */
	private Map<String, TranslationObjectsFromDB> createDictionaries() {
		TranslationObjectsFromDB objectsFromDB;

		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO,
							"Creating NE map for " + lang1 + ":" + language1 + "  to  " + lang2 + ":" + language2);

					// Creating NE Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("NE");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						BufferedReader bfr = new BufferedReader(
								new FileReader(new File((String) prop.get(NE_DICTIONARY_LOCATION) + "/"
										+ lang1.toLowerCase() + "To" + lang2.toLowerCase() + "Translation.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length >= 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										// spilitValueSynonyms = new
										// String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");
									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						bfr.close();
					} catch (FileNotFoundException e) {
						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " NEDICT");
					} catch (IOException e) {
						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "NEDICT",
							objectsFromDB);
					logger.log(Level.INFO, "Creating NE map for " + lang1 + ":" + language1 + "  to  " + lang2 + ":"
							+ language2 + " completed");

				}
			}
		}

		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO, "Creating Translation map for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2);

					// Creating Translation Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("TRANSLATION");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						BufferedReader bfr = new BufferedReader(
								new FileReader(new File((String) prop.get(TRANSLATION_DICTIONARY_LOCATION) + "/"
										+ lang1.toLowerCase() + "To" + lang2.toLowerCase() + "Translation.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length > 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										// spilitValueSynonyms = new
										// String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");
									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}

						bfr.close();
					} catch (FileNotFoundException e) {
						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " TRANSLATEDICT");
					} catch (IOException e) {
						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					logger.log(Level.INFO, lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "TRANSLATEDICT");
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "TRANSLATEDICT",
							objectsFromDB);
					logger.log(Level.INFO, "Creating Translation map for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2 + " completed");
				}
			}
		}

		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO, "Creating Transliteration map for " + lang1 + ":" + language1 + "  to  "
							+ lang2 + ":" + language2);

					// Creating Transliteration Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("TRANSLITERATION");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						BufferedReader bfr = new BufferedReader(
								new FileReader(new File((String) prop.get(TRANSLITERATION_DICTIONARY_LOCATION) + "/"
										+ lang1.toLowerCase() + "To" + lang2.toLowerCase() + "Transliteration.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length > 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										// spilitValueSynonyms = new
										// String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");
									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						bfr.close();
					} catch (FileNotFoundException e) {
						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " TRANSLITEDICT");

					} catch (IOException e) {

						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "TRANSLITEDICT",
							objectsFromDB);
					logger.log(Level.INFO, "Creating Transliteration map for " + lang1 + ":" + language1 + "  to  "
							+ lang2 + ":" + language2 + " completed");
				}
			}
		}
		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO, "Creating Wordnet Noun for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2);

					// Creating Wordnet Noun Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("WORDNETNOUN");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						// Wordnet lies at
						// $wordnet_home/Language1-Language2/abc.txt where
						// abc.txt can be from {adj,adv,noun,verb}
						// Take only noun and verb
						BufferedReader bfr = new BufferedReader(
								new FileReader(new File((String) prop.get(WORDNET_LOCATION) + "/" + language1 + "-"
										+ language2 + "/noun.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length > 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										// sspilitValueSynonyms = new
										// String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");

									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						bfr.close();
					} catch (FileNotFoundException e) {

						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " WORDNETNOUN");

					} catch (IOException e) {

						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "WORDNETNOUN",
							objectsFromDB);
					logger.log(Level.INFO, "Creating Wordnet Noun for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2 + " completed");
				}
			}
		}
		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO, "Creating Wordnet Verb for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2);

					// Creating Wordnet Verb Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("WORDNETVERB");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						// Wordnet lies at
						// $wordnet_home/Language1-Language2/abc.txt where
						// abc.txt can be from {adj,adv,noun,verb}
						// Take only noun and verb
						BufferedReader bfr = new BufferedReader(
								new FileReader(new File((String) prop.get(WORDNET_LOCATION) + "/" + language1 + "-"
										+ language2 + "/verb.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length > 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										spilitValueSynonyms = new String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");
									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						bfr.close();
					} catch (FileNotFoundException e) {

						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " WORDNETVERB");

					} catch (IOException e) {

						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "WORDNETVERB",
							objectsFromDB);
					logger.log(Level.INFO, "Creating Wordnet VERB for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2 + " completed");
				}
			}
		}
		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO, "Creating Wordnet Adjective for " + lang1 + ":" + language1 + "  to  "
							+ lang2 + ":" + language2);

					// Creating Wordnet Noun Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("WORDNETADJECTIVE");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						// Wordnet lies at
						// $wordnet_home/Language1-Language2/abc.txt where
						// abc.txt can be from {adj,adv,noun,verb}
						// Take only noun and verb
						BufferedReader bfr = new BufferedReader(new FileReader(new File(
								(String) prop.get(WORDNET_LOCATION) + "/" + language1 + "-" + language2 + "/adj.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length > 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										// sspilitValueSynonyms = new
										// String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");

									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						bfr.close();
					} catch (FileNotFoundException e) {

						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " WORDNETADJECTIIVE");

					} catch (IOException e) {

						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "WORDNETADJECTIVE",
							objectsFromDB);
					logger.log(Level.INFO, "Creating WORDNET ADJECTIVE for " + lang1 + ":" + language1 + "  to  "
							+ lang2 + ":" + language2 + " completed");
				}
			}
		}
		for (String lang1 : languageSupported) {
			for (String lang2 : languageSupported) {
				if (!lang1.equals(lang2)) {
					String language1 = languageCodesLAtoLANG.get(lang1);
					String language2 = languageCodesLAtoLANG.get(lang2);
					logger.log(Level.INFO, "Creating Wordnet Adverb for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2);

					// Creating Wordnet Noun Map object
					objectsFromDB = new TranslationObjectsFromDB();
					objectsFromDB.setSourceLanguage(language1);
					objectsFromDB.setTargetLanguage(language2);
					objectsFromDB.setCharacteristic("WORDNETADVERB");

					HashMap<String, LinkedHashSet<String>> hm = new HashMap<String, LinkedHashSet<String>>();
					try {
						// Wordnet lies at
						// $wordnet_home/Language1-Language2/abc.txt where
						// abc.txt can be from {adj,adv,noun,verb}
						// Take only noun and verb
						BufferedReader bfr = new BufferedReader(new FileReader(new File(
								(String) prop.get(WORDNET_LOCATION) + "/" + language1 + "-" + language2 + "/adv.txt")));
						String readline = "";
						while ((readline = bfr.readLine()) != null) {
							try {
								String[] spilitKeyValue = readline.trim().split(":");
								if (spilitKeyValue.length > 1) {
									String[] spilitValueSynonyms = null;

									try {
										String valueSynonyms = spilitKeyValue[1].trim();
										// sspilitValueSynonyms = new
										// String[valueSynonyms.split("@").length];
										spilitValueSynonyms = valueSynonyms.split("@");
										for (int i = 0; i < spilitValueSynonyms.length; i++)
											spilitValueSynonyms[i] = spilitValueSynonyms[i].replaceAll("_", " ");

									} catch (ArrayIndexOutOfBoundsException e) {
										logger.log(Level.INFO, "Readline:" + readline);
										continue;
									}
									if (hm.containsKey(spilitKeyValue[0])) {
										LinkedHashSet<String> templist = new LinkedHashSet<>(hm.get(spilitKeyValue[0]));
										templist.addAll(new LinkedHashSet<>(Arrays.asList(spilitValueSynonyms)));
										hm.put(spilitKeyValue[0].trim().toString(), templist);
									} else {
										hm.put(spilitKeyValue[0].trim().toString(),
												(LinkedHashSet<String>) new LinkedHashSet<>(
														Arrays.asList(spilitValueSynonyms)));
									}
								} else {
									continue;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						bfr.close();
					} catch (FileNotFoundException e) {

						logger.warning("File not Found for " + lang1 + ":" + language1 + " TO " + lang2 + ":"
								+ language2 + " WORDNETADVERB");

					} catch (IOException e) {

						logger.warning(e.getMessage());

					}
					objectsFromDB.setTranslationObjects(hm);
					mapOfTranslationObjects.put(lang1.toLowerCase() + "TO" + lang2.toLowerCase() + "WORDNETADVERB",
							objectsFromDB);
					logger.log(Level.INFO, "Creating Wordnet Adverb for " + lang1 + ":" + language1 + "  to  " + lang2
							+ ":" + language2 + " completed");
				}
			}
		}
		return mapOfTranslationObjects;
	}

	/**
	 * @param query
	 * @param passedLanguage
	 * @return Map<Word, Boolean> where Word is the created words, and boolean is
	 *         the status of translation
	 */
	private Map<Word, Boolean> getMapOfWordFromQuery(String query, String passedLanguage) {
		/* Intialize Word */

		Map<Word, Boolean> mapOfWordInQuery = new HashMap<>();
		if (!languageSupported.contains(passedLanguage)) {
			logger.log(Level.INFO, passedLanguage + " Language not supported yet!");
			return new HashMap<>();
		}

		String[] splitQuery = query.split(" ");
		TranslationObjectsFromDB dbofNE = mapOfTranslationObjects.get(passedLanguage.toLowerCase() + "TOenNEDICT");
		LinkedHashSet<String> listOfNE = new LinkedHashSet<>(dbofNE.getTranslationObjects().keySet());
		// We have a LinkedHashSet of all the NE objects for this language
		String[] splitQueryProcessed = new String[splitQuery.length];
		int k = 0;

		String wordCharacteristic = "";

		for (int i = 0; i < splitQuery.length; i++) {
			int j;
			String tempString = splitQuery[i];
			for (j = i + 1; j < splitQuery.length; j++) {

				if (listOfNE.contains(tempString + " " + splitQuery[j])) {
					i = j;
					wordCharacteristic = "NE";
					tempString += " " + splitQuery[j];
					continue;
				} else {
					break;
				}
			}

			splitQueryProcessed[k++] = tempString;
		}

		// splitQueryProcessed contains NE and other words seperately

		for (int i = 0; i < splitQueryProcessed.length; i++) {
			try {
				if (splitQueryProcessed[i] != null || !splitQueryProcessed[i].equals("")) {

					/*
					 * Detect the language from the word passed through language detector
					 */

					String detectedLanguage = passedLanguage;
					try {
						detector = DetectorFactory.create();
						detector.append(splitQueryProcessed[i].toString());

						detectedLanguage = detector.detect();
						System.out.println("Query of :" + splitQueryProcessed[i] + " passed Language: +"
								+ passedLanguage + " , Language identified: " + detectedLanguage);

					} catch (Exception e1) {
						e1.printStackTrace();
					}

					/* ends */
					if (detectedLanguage.equals("hi") && passedLanguage.equals("mr")) {
						detectedLanguage = passedLanguage;
					} else if (detectedLanguage.equals("mr") && passedLanguage.equals("hi")) {
						detectedLanguage = passedLanguage;
					}

					if (!languageSupported.contains(detectedLanguage)) {
						detectedLanguage = passedLanguage;
					}
					/* Find whether the word is NE, NORMAL, or STOPWORD */
					if (!wordCharacteristic.equalsIgnoreCase("NE")) {
						if (mapOfStopwords.containsKey(detectedLanguage)) {
							wordCharacteristic = mapOfStopwords.get(detectedLanguage).contains(splitQueryProcessed[i])
									? "STOPWORD"
									: "NORMAL";
						} else {
							wordCharacteristic = "NORMAL";
						}
					}
					String domain = getDomain(splitQueryProcessed[i]);

					Word word = new Word(splitQueryProcessed[i], detectedLanguage, wordCharacteristic,
							domain.equals("") ? "misc" : domain, "In Map Created");
					mapOfWordInQuery.put(word, false);

				}
			} catch (NullPointerException e) {
				continue;
			}
		}
		return mapOfWordInQuery;
	}

	private Map<Word, Boolean> getMapOfWordFromQuery(String query, String passedLanguage, boolean doNotSpiltQuery) {
		/* Intialize Word */

		Map<Word, Boolean> mapOfWordInQuery = new HashMap<>();
		if (!languageSupported.contains(passedLanguage)) {
			logger.log(Level.INFO, passedLanguage + " Language not supported yet!");
			return new HashMap<>();
		}
		String QueryProcessed = query;
		String wordCharacteristic = "";
		/*
		 * Detect the language from the word passed through language detector
		 */

		String detectedLanguage = passedLanguage;
		try {
			detector = DetectorFactory.create();
			detector.append(QueryProcessed.toString());

			detectedLanguage = detector.detect();
			System.out.println("Query of :" + QueryProcessed + " passed Language: +" + passedLanguage
					+ " , Language identified: " + detectedLanguage);

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		/* ends */
		if (detectedLanguage.equals("hi") && passedLanguage.equals("mr")) {
			detectedLanguage = passedLanguage;
		} else if (detectedLanguage.equals("mr") && passedLanguage.equals("hi")) {
			detectedLanguage = passedLanguage;
		}

		if (!languageSupported.contains(detectedLanguage)) {
			detectedLanguage = passedLanguage;
		}

		String domain = getDomain(QueryProcessed);

		Word word = new Word(QueryProcessed, detectedLanguage, "Skipping", domain.equals("") ? "misc" : domain,
				"In Map Created");
		mapOfWordInQuery.put(word, false);

		return mapOfWordInQuery;

	}

	/**
	 * @param query
	 * @param passedLanguage
	 * @return
	 */
	private LinkedHashSet<Word> getListOfWordFromQuery(String query, String passedLanguage) {
		/* Intialize Word */

		// Map<Word, Boolean> mapOfWordInQuery = new HashMap<>();
		LinkedHashSet<Word> listOfWordInQuery = new LinkedHashSet<>();
		if (!languageSupported.contains(passedLanguage)) {
			logger.log(Level.INFO, passedLanguage + " Language not supported yet!");
			return new LinkedHashSet<>();
		}

		String[] splitQuery = query.split(" ");
		TranslationObjectsFromDB dbofNE = mapOfTranslationObjects.get(passedLanguage.toLowerCase() + "TOenNEDICT");
		LinkedHashSet<String> listOfNE = new LinkedHashSet<>(dbofNE.getTranslationObjects().keySet());
		// We have a LinkedHashSet of all the NE objects for this language
		String[] splitQueryProcessed = new String[splitQuery.length];
		int k = 0;
		String wordCharacteristic = "";

		for (int i = 0; i < splitQuery.length; i++) {
			int j;
			String tempString = splitQuery[i];
			for (j = i + 1; j < splitQuery.length; j++) {

				if (listOfNE.contains(tempString + " " + splitQuery[j])) {
					i = j;
					wordCharacteristic = "NE";
					tempString += " " + splitQuery[j];
					continue;
				} else {
					break;
				}
			}

			splitQueryProcessed[k++] = tempString;
		}
		// splitQueryProcessed contains NE and other words seperately

		for (int i = 0; i < splitQueryProcessed.length; i++) {
			try {
				if (splitQueryProcessed[i] != null || !splitQueryProcessed[i].equals("")) {

					/*
					 * Detect the language from the word passed through language detector
					 */
					String detectedLanguage = passedLanguage;
					try {
						detector = DetectorFactory.create();
						detector.append(splitQueryProcessed[i].toString());

						detectedLanguage = detector.detect();
						System.out.println("Query of :" + splitQueryProcessed[i] + " passed Language: +"
								+ passedLanguage + " , Language identified: " + detectedLanguage);

					} catch (LangDetectException e1) {
						e1.printStackTrace();
						detectedLanguage = passedLanguage;
					}

					/* ends */
					if (detectedLanguage.equals("hi") && passedLanguage.equals("mr")) {
						detectedLanguage = passedLanguage;
					} else if (detectedLanguage.equals("mr") && passedLanguage.equals("hi")) {
						detectedLanguage = passedLanguage;
					}

					if (!languageSupported.contains(detectedLanguage)) {
						detectedLanguage = passedLanguage;
					}
					/* Find whether the word is NE, NORMAL, or STOPWORD */
					if (!wordCharacteristic.equalsIgnoreCase("NE")) {
						if (mapOfStopwords.containsKey(detectedLanguage)) {
							wordCharacteristic = mapOfStopwords.get(detectedLanguage).contains(splitQueryProcessed[i])
									? "STOPWORD"
									: "NORMAL";
						} else {
							wordCharacteristic = "NORMAL";
						}
					}
					String domain = getDomain(splitQueryProcessed[i]);

					Word word = new Word(splitQueryProcessed[i], detectedLanguage, wordCharacteristic,
							domain.equals("") ? "misc" : domain, "In Map Created");
					listOfWordInQuery.add(word);

				}
			} catch (NullPointerException e) {
				continue;
			}
		}
		return listOfWordInQuery;
	}

	private LinkedHashSet<Word> getListOfWordFromQuery(String query, String passedLanguage, boolean doNotSpilit) {
		/* Intialize Word */

		// Map<Word, Boolean> mapOfWordInQuery = new HashMap<>();
		LinkedHashSet<Word> listOfWordInQuery = new LinkedHashSet<>();
		if (!languageSupported.contains(passedLanguage)) {
			logger.log(Level.INFO, passedLanguage + " Language not supported yet!");
			return new LinkedHashSet<>();
		}

		String QueryProcessed = query;

		String wordCharacteristic = "Skipping";

		String detectedLanguage = passedLanguage;
		try {
			detector = DetectorFactory.create();
			detector.append(QueryProcessed.toString());

			detectedLanguage = detector.detect();
			System.out.println("Query of :" + QueryProcessed + " passed Language: +" + passedLanguage
					+ " , Language identified: " + detectedLanguage);

		} catch (LangDetectException e1) {
			e1.printStackTrace();
			detectedLanguage = passedLanguage;
		}

		/* ends */
		if (detectedLanguage.equals("hi") && passedLanguage.equals("mr")) {
			detectedLanguage = passedLanguage;
		} else if (detectedLanguage.equals("mr") && passedLanguage.equals("hi")) {
			detectedLanguage = passedLanguage;
		}

		if (!languageSupported.contains(detectedLanguage)) {
			detectedLanguage = passedLanguage;
		}
		/* Find whether the word is NE, NORMAL, or STOPWORD */
		if (!wordCharacteristic.equalsIgnoreCase("NE")) {
			if (mapOfStopwords.containsKey(detectedLanguage)) {
				wordCharacteristic = mapOfStopwords.get(detectedLanguage).contains(QueryProcessed) ? "STOPWORD"
						: "NORMAL";
			} else {
				wordCharacteristic = "NORMAL";
			}
		}
		String domain = getDomain(QueryProcessed);

		Word word = new Word(QueryProcessed, detectedLanguage, wordCharacteristic, domain.equals("") ? "misc" : domain,
				"In Map Created");
		listOfWordInQuery.add(word);

		return listOfWordInQuery;
	}

	/**
	 * @param passedWord
	 * @param sourceLang
	 * @param targetLang
	 * @return LinkedHashSet<String> of translations for passedWord
	 */
	private LinkedHashSet<String> getTranslationFromSourceToTarget(Word passedWord, String sourceLang,
			String targetLang) {

		Map<String, LinkedHashSet<String>> translationObjectForWord = new HashMap<>();
		TranslationObjectsFromDB dbObject = null;
		Map<String, LinkedHashSet<String>> translationObjectsFromDb = new HashMap<>();
		logger.log(Level.INFO,
				"sourceLanguage is : " + sourceLang + " identified word language is :" + passedWord.getLanguage());
		switch (passedWord.getCharacteristic()) {
		case "NE":
			// NE identified, only dictionary lookup and transliteration
			logger.log(Level.INFO, "for NE :" + passedWord);
			dbObject = mapOfTranslationObjects
					.get(passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase() + "NEDICT");
			logger.log(Level.INFO, passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase()
					+ "NEDICT size: " + dbObject.getTranslationObjects().size());
			// dbObjectInited
			translationObjectsFromDb = dbObject.getTranslationObjects();

			if (translationObjectsFromDb.containsKey(passedWord.getWord().toLowerCase())) {
				{ // NE Word found

					passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
							+ targetLang.toLowerCase() + "NEDICT");
					debugWordList.add(passedWord);

					translationObjectForWord.put(targetLang.toLowerCase(),
							translationObjectsFromDb.get(passedWord.getWord()));

				}
			}
			// IF integrate WORDNET too for NE

			else {
				{
					LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
					try {
						String s = queryTranslationService.RPCCall(passedWord.getLanguage(), targetLang,
								passedWord.getWord());
						// String s =
						// ProcessingDao.externalResponseForTranslation(passedWord.getLanguage(),
						// targetLang,passedWord.getWord());
						passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
								+ targetLang.toLowerCase() + "_NE_MOSES");
						/*
						 * passedWord.setDebug(passedWord.getDebug() + "--" +
						 * passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase() +
						 * "_NE_EXTERNALSERVICE");
						 */
						debugWordList.add(passedWord);
						LinkedHashSet.add(s);
						translationObjectForWord.put(targetLang.toLowerCase(), LinkedHashSet);
					} catch (Exception e) {
						e.printStackTrace();
						translationObjectForWord.put(targetLang.toLowerCase(), new LinkedHashSet<>());
					}
				}
			}
			logger.log(Level.INFO, "for NE -- after processing:" + translationObjectForWord);
			break;
		case "STOPWORD":// Stopword identified, only transliteraion TODO Need to
						// be removed
			logger.log(Level.INFO, "for STOPWORD :" + passedWord); {
			LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
			try {
				String s = queryTranslationService.RPCCall(passedWord.getLanguage(), targetLang, passedWord.getWord());
				// String s =
				// ProcessingDao.externalResponseForTranslation(passedWord.getLanguage(),
				// targetLang, passedWord.getWord());
				passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
						+ targetLang.toLowerCase() + "_STOPWORD_MOSES");
				/*
				 * passedWord.setDebug(passedWord.getDebug() + "--" +
				 * passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase() +
				 * "_STOPWORD_EXTERNALSERVICES");
				 */
				debugWordList.add(passedWord);
				LinkedHashSet.add(s);
				/* Removing stopword Transliteration Call for NVLI */

				// translationObjectForWord.put(targetLang.toLowerCase(), LinkedHashSet);
				translationObjectForWord.put(targetLang.toLowerCase(), new LinkedHashSet<>());
			} catch (Exception e) {
				e.printStackTrace();
				translationObjectForWord.put(targetLang.toLowerCase(), new LinkedHashSet<>());
			}
		}
			logger.log(Level.INFO, "for STOPWORD -- after processing:" + translationObjectForWord);
			break;
		case "NORMAL": // NORMAL, WORDNET lookup and transliteration
			logger.log(Level.INFO, "for NORMAL :" + passedWord);
			// Trying WORDNETNOUN

			dbObject = mapOfTranslationObjects
					.get(passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase() + "WORDNETNOUN");
			logger.log(Level.INFO, passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase()
					+ "WORDNETNOUN size: " + dbObject.getTranslationObjects().size());
			// dbObjectInited

			try {
				translationObjectsFromDb = dbObject.getTranslationObjects();

				if (translationObjectsFromDb.containsKey(passedWord.getWord().toLowerCase())) {
					logger.log(Level.INFO, "Normal wordnet: " + passedWord.getWord());
					passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
							+ targetLang.toLowerCase() + "_NORMAL_WORDNET_NOUN");
					debugWordList.add(passedWord);
					translationObjectForWord.put(targetLang.toLowerCase(),
							translationObjectsFromDb.get(passedWord.getWord().toLowerCase()));

				} else {
					// Trying WORDNETNOUN
					dbObject = mapOfTranslationObjects.get(
							passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase() + "WORDNETVERB");
					// dbObjectInited
					translationObjectsFromDb = dbObject.getTranslationObjects();
					logger.log(Level.INFO, passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase()
							+ "WORDNETVERB size: " + dbObject.getTranslationObjects().size());
					if (translationObjectsFromDb.containsKey(passedWord.getWord().toLowerCase())) {
						logger.log(Level.INFO, "Normal wordnet: " + passedWord.getWord());
						passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
								+ targetLang.toLowerCase() + "_NORMAL_WORDNET_VERB");
						debugWordList.add(passedWord);
						translationObjectForWord.put(targetLang.toLowerCase(),
								translationObjectsFromDb.get(passedWord.getWord().toLowerCase()));

					} else {
						dbObject = mapOfTranslationObjects.get(passedWord.getLanguage().toLowerCase() + "TO"
								+ targetLang.toLowerCase() + "WORDNETADJECTIVE");
						// dbObjectInited
						translationObjectsFromDb = dbObject.getTranslationObjects();
						logger.log(Level.INFO, passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase()
								+ "WORDNETADJECTIVE size: " + dbObject.getTranslationObjects().size());
						if (translationObjectsFromDb.containsKey(passedWord.getWord().toLowerCase())) {
							logger.log(Level.INFO, "Normal wordnet: " + passedWord.getWord());
							passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase()
									+ "TO" + targetLang.toLowerCase() + "_NORMAL_WORDNET_ADJECTIVE");
							debugWordList.add(passedWord);
							translationObjectForWord.put(targetLang.toLowerCase(),
									translationObjectsFromDb.get(passedWord.getWord().toLowerCase()));

						} else {
							dbObject = mapOfTranslationObjects.get(passedWord.getLanguage().toLowerCase() + "TO"
									+ targetLang.toLowerCase() + "WORDNETADVERB");
							// dbObjectInited
							translationObjectsFromDb = dbObject.getTranslationObjects();
							logger.log(Level.INFO,
									passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase()
											+ "WORDNETADVERB size: " + dbObject.getTranslationObjects().size());
							if (translationObjectsFromDb.containsKey(passedWord.getWord().toLowerCase())) {
								logger.log(Level.INFO, "Normal wordnet: " + passedWord.getWord());
								passedWord
										.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase()
												+ "TO" + targetLang.toLowerCase() + "_NORMAL_WORDNET_ADVERB");
								debugWordList.add(passedWord);
								translationObjectForWord.put(targetLang.toLowerCase(),
										translationObjectsFromDb.get(passedWord.getWord().toLowerCase()));

							} else {

								if (queryTranslationService != null) {
									LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
									try {
										logger.log(Level.INFO, "Normal RPCcall: " + passedWord.getWord());
										String s = queryTranslationService.RPCCall(passedWord.getLanguage(), targetLang,
												passedWord.getWord());
										/*
										 * String s = ProcessingDao.externalResponseForTranslation(
										 * passedWord.getLanguage(), targetLang, passedWord.getWord());
										 */
										passedWord.setDebug(
												passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase()
														+ "TO" + targetLang.toLowerCase() + "_NORMAL_MOSES");
										/*
										 * passedWord.setDebug( passedWord.getDebug() + "--" +
										 * passedWord.getLanguage().toLowerCase() + "TO" + targetLang.toLowerCase() +
										 * "_NORMAL_EXTERNALSERVICE");
										 */
										debugWordList.add(passedWord);
										LinkedHashSet.add(s);
										translationObjectForWord.put(targetLang.toLowerCase(), LinkedHashSet);
									} catch (Exception e) {
										e.printStackTrace();
										try {
											logger.log(Level.INFO,
													"Normal RPCcall (Locally through moses): " + passedWord.getWord());
											String s = queryTranslationService.RPCCall(passedWord.getLanguage(),
													targetLang, passedWord.getWord());
											passedWord.setDebug(passedWord.getDebug() + "--"
													+ passedWord.getLanguage().toLowerCase() + "TO"
													+ targetLang.toLowerCase() + "_MOSES");
											debugWordList.add(passedWord);
											LinkedHashSet.add(s);
											translationObjectForWord.put(targetLang.toLowerCase(), LinkedHashSet);
										} catch (Exception e1) {
											e1.printStackTrace();
											translationObjectForWord.put(targetLang.toLowerCase(),
													new LinkedHashSet<>());
										}

									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Everything failed, trying queryTranslationService :" + e.getMessage());
				if (queryTranslationService != null) {
					LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
					try {
						logger.log(Level.INFO, "Normal RPCcall: " + passedWord.getWord());
						String s = queryTranslationService.RPCCall(passedWord.getLanguage(), targetLang,
								passedWord.getWord());
						passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
								+ targetLang.toLowerCase() + "_UNKNOWN_MOSES");
						debugWordList.add(passedWord);
						LinkedHashSet.add(s);
						translationObjectForWord.put(targetLang.toLowerCase(), LinkedHashSet);
					} catch (Exception e1) {
						e1.printStackTrace();
						translationObjectForWord.put(targetLang.toLowerCase(), new LinkedHashSet<>());
					}
				}
			}
			logger.log(Level.INFO, "for NORMAL -- after processing:" + translationObjectForWord);
			break;
		default:
		}
		System.out.println(translationObjectForWord);
		return translationObjectForWord.get(targetLang);

	}

	private String getTranslationUsingApi(String passedstring, String sourceLang, String targetLang) {
		String translation = "";
		URI uri = null;
		try {

			uri = new URI("http://" + EXTERNAL_API_HOSTNAME + ":" + EXTERNAL_API_PORT + "/webservice/gettranslation/"
					+ sourceLang.toLowerCase() + "/" + targetLang.toLowerCase() + "/"
					+ passedstring.replaceAll(" ", "%20"));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpGet httpGet = new HttpGet(uri);
			httpGet.addHeader("accept", "application/json");

			System.out.println("url:" + uri.toString());
			HttpResponse httpResponse = httpClient.execute(httpGet);
			String output = httpResponse.toString();
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			String line = "";
			String builder = new String();
			while ((line = rd.readLine()) != null) {
				builder += line;
				// builder+="\n";
			}
			System.out.println("Output from Server .... \n" + output);
			System.out.println("builder : " + builder);
			/*
			 * while ((output = br.readLine()) != null) { System.out.println(output); }
			 */
			translation = builder.replaceAll("\"", "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return translation;
	}

	private LinkedHashSet<String> getTranslationFromSourceToTarget(Word passedWord, String sourceLang,
			String targetLang, boolean useLocalApi) {
		Map<String, LinkedHashSet<String>> translationObjectForWord = new HashMap<>();
		logger.log(Level.INFO,
				"sourceLanguage is : " + sourceLang + " identified word language is :" + passedWord.getLanguage());
		LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();

		try {
			URI uri = null;
			try {

				uri = new URI("http://" + EXTERNAL_API_HOSTNAME + ":" + EXTERNAL_API_PORT
						+ "/webservice/gettranslation/" + sourceLang.toLowerCase() + "/" + targetLang.toLowerCase()
						+ "/" + passedWord.getWord().replaceAll(" ", "%20"));
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpGet httpGet = new HttpGet(uri);
			httpGet.addHeader("accept", "application/json");

			System.out.println("url:" + uri.toString());
			HttpResponse httpResponse = httpClient.execute(httpGet);
			String output = httpResponse.toString();
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			String line = "";
			String builder = new String();
			while ((line = rd.readLine()) != null) {
				builder += line;
				// builder+="\n";
			}
			System.out.println("Output from Server .... \n" + output);
			System.out.println("builder : " + builder);
			/*
			 * while ((output = br.readLine()) != null) { System.out.println(output); }
			 */
			LinkedHashSet.add(builder.replaceAll("\"", ""));
			passedWord.setDebug(passedWord.getDebug() + "--" + passedWord.getLanguage().toLowerCase() + "TO"
					+ targetLang.toLowerCase() + "_EXTERNAL_API_CALL");
			debugWordList.add(passedWord);

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		}
		translationObjectForWord.put(targetLang.toLowerCase(), LinkedHashSet);
		return translationObjectForWord.get(targetLang);
	}

	/**
	 * @param translatedMap
	 * @param arrangedListOfQuery
	 * @return
	 */
	private Map<String, LinkedHashSet<Word>> arrangeQueryMap(Map<String, LinkedHashSet<Word>> translatedMap,
			LinkedHashSet<Word> arrangedListOfQuery) {
		/* Not working: NE LinkedHashSet identification */

		Map<String, LinkedHashSet<Word>> map = new HashMap<>();

		LinkedHashSet<Word> translatedWordList = new LinkedHashSet<>();
		for (String lang : translatedMap.keySet()) {
			LinkedHashSet<Word> translatedWordListArranged = new LinkedHashSet<>();
			translatedWordList = (LinkedHashSet<Word>) translatedMap.get(lang);
			for (Word arrangedWord : arrangedListOfQuery) {
				for (Word w : translatedWordList) {
					if (w.getWord().equals(arrangedWord.getWord())) {
						translatedWordListArranged.add(w);
					}
				}
			}
			map.put(lang, translatedWordListArranged);

		}
		return map;
	}

	/**
	 * @param passedLanguage
	 * @param searchElement
	 * @return Map<String, LinkedHashSet<Word>>
	 * @throws Exception
	 */
	public Map<String, LinkedHashSet<Word>> getCrossLingualMapFromSearchQuery(String passedLanguage,
			String searchElement) {
		Map<String, LinkedHashSet<Word>> map = new HashMap<>();

		// Get the language codes from serverconfig, that are supported in this
		// version
		if (passedLanguage.equals("en")) {
			logger.log(Level.SEVERE, "English to IL not available!");
			return new HashMap<>();
		}

		if (!languageSupported.contains(passedLanguage)) {
			logger.log(Level.SEVERE, passedLanguage + " Language not supported yet!");
			return new HashMap<>();
		}
		Map<Word, Boolean> mapOfWordFromSearchElement = getMapOfWordFromQuery(searchElement, passedLanguage);
		if (mapOfWordFromSearchElement == null || mapOfWordFromSearchElement.isEmpty()) {
			logger.log(Level.SEVERE, passedLanguage + " translation will probably fail, hence escaping");
			return new HashMap<>();
		}
		// Default passed language added in map
		map.put(passedLanguage, new LinkedHashSet<>(getMapOfWordFromQuery(searchElement, passedLanguage).keySet()));

		// Translate to english
		TranslationBean translationBeanEnglish = new TranslationBean();
		translationBeanEnglish.setSourceLanguage(passedLanguage);
		translationBeanEnglish.setTargetLanguage("en");
		translationBeanEnglish.setSourceLanguageElement(searchElement);
		LinkedHashSet<Word> listOfWordsFromMap = new LinkedHashSet<>(mapOfWordFromSearchElement.keySet());

		map.put("en", new LinkedHashSet<Word>());
		for (Word word : listOfWordsFromMap) {
			HashMap<String, LinkedHashSet<String>> hm = new HashMap<>();
			LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
			LinkedHashSet = getTranslationFromSourceToTarget(word, passedLanguage, "en");
			System.out.println("en " + LinkedHashSet);

			hm.put("en", LinkedHashSet);
			word.setTranslations(hm);
			if (!LinkedHashSet.isEmpty())
				mapOfWordFromSearchElement.put(word, true);
			System.out.println("en 2 " + word);
		}
		map.put("en", new LinkedHashSet<>(mapOfWordFromSearchElement.keySet()));
		System.out.println(map);
		// Completed
		Map<Word, Boolean> mapOfWordFromSearchElementForHi = getMapOfWordFromQuery(searchElement, passedLanguage);
		if (mapOfWordFromSearchElementForHi == null || mapOfWordFromSearchElementForHi.isEmpty()) {
			logger.log(Level.SEVERE, passedLanguage + " translation will probably fail, hence escaping");
			return new HashMap<>();
		}
		if (!passedLanguage.equalsIgnoreCase("hi")) {
			// Translate to hindi
			map.put("hi", new LinkedHashSet<Word>());
			TranslationBean translationBeanHi = new TranslationBean();
			translationBeanHi.setSourceLanguage(passedLanguage);
			translationBeanHi.setTargetLanguage("hi");
			translationBeanHi.setSourceLanguageElement(searchElement);
			LinkedHashSet<Word> listOfWordsFromMapHi = new LinkedHashSet<>(mapOfWordFromSearchElementForHi.keySet());

			for (Word word : listOfWordsFromMapHi) {
				HashMap<String, LinkedHashSet<String>> hmHi = new HashMap<>();
				LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
				LinkedHashSet = getTranslationFromSourceToTarget(word, passedLanguage, "hi");

				hmHi.put("hi", LinkedHashSet);
				word.setTranslations(hmHi);
				if (!LinkedHashSet.isEmpty())
					mapOfWordFromSearchElementForHi.put(word, true);
			}
			map.put("hi", new LinkedHashSet<>(mapOfWordFromSearchElementForHi.keySet()));
			// Completed
		}
		/* arrange return map on basis of query */
		logger.log(Level.INFO, "map before: " + map);
		LinkedHashSet<Word> arrangedListOfQuery = getListOfWordFromQuery(searchElement, passedLanguage);
		map = arrangeQueryMap(map, arrangedListOfQuery);
		logger.log(Level.INFO, "map after: " + map);
		return map;
	}

	public Map<String, LinkedHashSet<Word>> getCrossLingualMapFromSearchQuery(String passedLanguage,
			String searchElement, boolean useApi) {
		Map<String, LinkedHashSet<Word>> map = new HashMap<>();

		// Get the language codes from serverconfig, that are supported in this
		// version
		if (passedLanguage.equals("en")) {
			logger.log(Level.SEVERE, "English to IL not available!");
			return new HashMap<>();
		}

		if (!languageSupported.contains(passedLanguage)) {
			logger.log(Level.SEVERE, passedLanguage + " Language not supported yet!");
			return new HashMap<>();
		}
		Map<Word, Boolean> mapOfWordFromSearchElement = getMapOfWordFromQuery(searchElement, passedLanguage, true);
		map.put(passedLanguage,
				new LinkedHashSet<>(getMapOfWordFromQuery(searchElement, passedLanguage, true).keySet()));
		// Translate to english
		TranslationBean translationBeanEnglish = new TranslationBean();
		translationBeanEnglish.setSourceLanguage(passedLanguage);
		translationBeanEnglish.setTargetLanguage("en");
		translationBeanEnglish.setSourceLanguageElement(searchElement);
		LinkedHashSet<Word> listOfWordsFromMap = new LinkedHashSet<>(mapOfWordFromSearchElement.keySet());

		map.put("en", new LinkedHashSet<Word>());
		for (Word word : listOfWordsFromMap) {
			HashMap<String, LinkedHashSet<String>> hm = new HashMap<>();
			LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
			LinkedHashSet = getTranslationFromSourceToTarget(word, passedLanguage, "en", true);
			System.out.println("en " + LinkedHashSet);

			hm.put("en", LinkedHashSet);
			word.setTranslations(hm);
			if (!LinkedHashSet.isEmpty())
				mapOfWordFromSearchElement.put(word, true);
			System.out.println("en 2 " + word);
		}
		map.put("en", new LinkedHashSet<>(mapOfWordFromSearchElement.keySet()));
		System.out.println(map);
		// Completed
		Map<Word, Boolean> mapOfWordFromSearchElementForHi = getMapOfWordFromQuery(searchElement, passedLanguage, true);
		if (mapOfWordFromSearchElementForHi == null || mapOfWordFromSearchElementForHi.isEmpty()) {
			logger.log(Level.SEVERE, passedLanguage + " translation will probably fail, hence escaping");
			return new HashMap<>();
		}
		if (!passedLanguage.equalsIgnoreCase("hi")) {
			// Translate to hindi
			map.put("hi", new LinkedHashSet<Word>());
			TranslationBean translationBeanHi = new TranslationBean();
			translationBeanHi.setSourceLanguage(passedLanguage);
			translationBeanHi.setTargetLanguage("hi");
			translationBeanHi.setSourceLanguageElement(searchElement);
			LinkedHashSet<Word> listOfWordsFromMapHi = new LinkedHashSet<>(mapOfWordFromSearchElementForHi.keySet());

			for (Word word : listOfWordsFromMapHi) {
				HashMap<String, LinkedHashSet<String>> hmHi = new HashMap<>();
				LinkedHashSet<String> LinkedHashSet = new LinkedHashSet<>();
				LinkedHashSet = getTranslationFromSourceToTarget(word, passedLanguage, "hi", true);

				hmHi.put("hi", LinkedHashSet);
				word.setTranslations(hmHi);
				if (!LinkedHashSet.isEmpty())
					mapOfWordFromSearchElementForHi.put(word, true);
			}
			map.put("hi", new LinkedHashSet<>(mapOfWordFromSearchElementForHi.keySet()));
			// Completed
		}
		/* arrange return map on basis of query */
		logger.log(Level.INFO, "map before: " + map);
		LinkedHashSet<Word> arrangedListOfQuery = getListOfWordFromQuery(searchElement, passedLanguage, true);
		map = arrangeQueryMap(map, arrangedListOfQuery);
		logger.log(Level.INFO, "map after: " + map);
		return map;
	}

	/**
	 * @param sourcelanguage
	 * @param targetLanguage
	 * @param searchElement
	 * @return Map<String, LinkedHashSet<Word>>
	 * @throws Exception
	 */
	public Map<String, LinkedHashSet<Word>> getInterCrossLingualMapFromSearchQuery(String sourcelanguage,
			String targetLanguage, String searchElement) {
		if (sourcelanguage.equalsIgnoreCase(targetLanguage)) {
			logger.log(Level.SEVERE, "Source and Destination languages are same!");
			return new HashMap<>();
		}
		if (sourcelanguage.equalsIgnoreCase("en")) {
			logger.log(Level.SEVERE, "English to IL not available!");
			return new HashMap<>();
		}
		if (!languageSupported.contains(targetLanguage)) {
			logger.log(Level.SEVERE, targetLanguage + " Language not supported yet!");
			return new HashMap<>();
		}
		Map<String, LinkedHashSet<Word>> map = new HashMap<>();

		// No need in Intercrosslingual
		// map.put(sourcelanguage, new
		// LinkedHashSet<>(getMapOfWordFromQuery(searchElement,
		// sourcelanguage).keySet()));
		if (sourcelanguage.equalsIgnoreCase("hi")) {

			TranslationBean translationBean = new TranslationBean();
			translationBean.setSourceLanguage(sourcelanguage);
			translationBean.setTargetLanguage(targetLanguage);
			translationBean.setSourceLanguageElement(searchElement);
			Map<Word, Boolean> mapOfWordFromSearchElement = getMapOfWordFromQuery(searchElement, sourcelanguage);
			LinkedHashSet<Word> listOfWordsFromMap = new LinkedHashSet<>(mapOfWordFromSearchElement.keySet());

			for (Word word : listOfWordsFromMap) {
				HashMap<String, LinkedHashSet<String>> hm = new HashMap<>();
				LinkedHashSet<String> listInTargetLanguage = new LinkedHashSet<>();
				listInTargetLanguage = getTranslationFromSourceToTarget(word, sourcelanguage, targetLanguage);
				hm.put(targetLanguage, listInTargetLanguage);
				word.setTranslations(hm);
				if (!listInTargetLanguage.isEmpty())
					mapOfWordFromSearchElement.put(word, true);
			}
			map.put(targetLanguage, new LinkedHashSet<>(mapOfWordFromSearchElement.keySet()));

		} else {

			TranslationBean translationBean = new TranslationBean();
			translationBean.setSourceLanguage(sourcelanguage);
			translationBean.setTargetLanguage(targetLanguage);
			translationBean.setSourceLanguageElement(searchElement);
			Map<Word, Boolean> mapOfWordFromSearchElement = getMapOfWordFromQuery(searchElement, sourcelanguage);
			LinkedHashSet<Word> listOfWordsFromMap = new LinkedHashSet<>(mapOfWordFromSearchElement.keySet());

			for (Word word : listOfWordsFromMap) {
				HashMap<String, LinkedHashSet<String>> hm = new HashMap<>();
				LinkedHashSet<String> listInHindi = new LinkedHashSet<>();
				LinkedHashSet<String> listInTargetLanguage = new LinkedHashSet<>();
				listInHindi = getTranslationFromSourceToTarget(word, sourcelanguage, "hi");
				for (String wordTranslationInHindi : listInHindi) {
					Word w = new Word(wordTranslationInHindi, "hi", word.getCharacteristic(), "", word.getDebug());
					listInTargetLanguage = getTranslationFromSourceToTarget(w, "hi", targetLanguage);
				}
				hm.put(targetLanguage, listInTargetLanguage);
				word.setTranslations(hm);
				if (!listInTargetLanguage.isEmpty())
					mapOfWordFromSearchElement.put(word, true);
			}
			map.put(targetLanguage, new LinkedHashSet<>(mapOfWordFromSearchElement.keySet()));
		}
		/* arrange return map on basis of query */
		// logger.log(Level.INFO, "map before: " + map);
		LinkedHashSet<Word> arrangedListOfQuery = getListOfWordFromQuery(searchElement, sourcelanguage);
		map = arrangeQueryMap(map, arrangedListOfQuery);
		// logger.log(Level.INFO, "map after: " + map);
		return map;
	}

	public Map<String, LinkedHashSet<Word>> getInterCrossLingualMapFromSearchQuery(String sourcelanguage,
			String targetLanguage, String searchElement, boolean useApi) {
		if (sourcelanguage.equalsIgnoreCase(targetLanguage)) {
			logger.log(Level.SEVERE, "Source and Destination languages are same!");
			return new HashMap<>();
		}
		if (sourcelanguage.equalsIgnoreCase("en")) {
			logger.log(Level.SEVERE, "English to IL not available!");
			return new HashMap<>();
		}
		if (!languageSupported.contains(targetLanguage)) {
			logger.log(Level.SEVERE, targetLanguage + " Language not supported yet!");
			return new HashMap<>();
		}
		Map<String, LinkedHashSet<Word>> map = new HashMap<>();

		TranslationBean translationBean = new TranslationBean();
		translationBean.setSourceLanguage(sourcelanguage);
		translationBean.setTargetLanguage(targetLanguage);
		translationBean.setSourceLanguageElement(searchElement);
		Map<Word, Boolean> mapOfWordFromSearchElement = getMapOfWordFromQuery(searchElement, sourcelanguage, true);
		LinkedHashSet<Word> listOfWordsFromMap = new LinkedHashSet<>(mapOfWordFromSearchElement.keySet());

		for (Word word : listOfWordsFromMap) {
			HashMap<String, LinkedHashSet<String>> hm = new HashMap<>();
			LinkedHashSet<String> listInTargetLanguage = new LinkedHashSet<>();
			listInTargetLanguage = getTranslationFromSourceToTarget(word, sourcelanguage, targetLanguage, true);
			hm.put(targetLanguage, listInTargetLanguage);
			word.setTranslations(hm);
			if (!listInTargetLanguage.isEmpty())
				mapOfWordFromSearchElement.put(word, true);
		}
		map.put(targetLanguage, new LinkedHashSet<>(mapOfWordFromSearchElement.keySet()));

		/* arrange return map on basis of query */
		logger.log(Level.INFO, "map before: " + map);
		LinkedHashSet<Word> arrangedListOfQuery = getListOfWordFromQuery(searchElement, sourcelanguage, true);
		map = arrangeQueryMap(map, arrangedListOfQuery);
		logger.log(Level.INFO, "map after: " + map);
		return map;
	}

	/**
	 * @param searchElement
	 * @return Detected language code in two characters
	 */
	public String getLanguage(String searchElement) {
		String detectedLanguage = "";
		try {
			detector = DetectorFactory.create();
			detector.append(searchElement.toString());
			detectedLanguage = detector.detect();
		} catch (LangDetectException e) {
			e.printStackTrace();
		}
		return detectedLanguage;
	}

	/**
	 * @param dateformat
	 * @return String
	 * @throws ParseException
	 */
	public String getDate(String dateformat) throws ParseException {
		logger.log(Level.INFO, "publish Date is " + dateformat);
		SimpleDateFormat oldSdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH); // Tue
																											// May
																											// 02
																											// 10:31:00
																											// IST
																											// 2017
		SimpleDateFormat newSdf = new SimpleDateFormat("MMM dd, yyyy");
		Date olddate = (Date) oldSdf.parse(dateformat);
		return newSdf.format(olddate);
	}

	public String getDomain(String wordString) {
		// Domain Identification

		String mahoutDomainData = MAHOUT_DOMAIN_DATA_PATH;
		String modelDomainPath = mahoutDomainData + "/model";
		String labelDomainIndexPath = modelDomainPath + "/labelindex";
		String dictionaryDomainPath = mahoutDomainData + "/data-vectors/dictionary.file-0";
		String documentDomainFrequencyPath = mahoutDomainData + "/data-vectors/df-count/part-r-00000";
		String docsDomainPath = "lovey";
		String nbCategoryFromTitle = "";
		String nbCategoryFromContent = "";
		if (wordString != null && wordString.equals("") == false) {
			nbCategoryFromTitle = classifyDoc(modelDomainPath, labelDomainIndexPath, dictionaryDomainPath,
					documentDomainFrequencyPath, docsDomainPath, wordString);
		}

		if (nbCategoryFromTitle.equals(nbCategoryFromContent)) {
			return nbCategoryFromContent; // Return anything
		} else
			return nbCategoryFromContent; // Return content domain only

	}

	// CATEGORIZATION FILTER Code Modules starts here!!
	// --date:Aug7,2015

	// domain identification using Naive bayes model....

	/**
	 * @param modelPath
	 * @param labelIndexPath
	 * @param dictionaryPath
	 * @param documentFrequencyPath
	 * @param docsPath
	 * @param doc
	 * @return
	 */
	public static String classifyDoc(String modelPath, String labelIndexPath, String dictionaryPath,
			String documentFrequencyPath, String docsPath, String doc) {
		int bestCategoryId = -1;
		Map<Integer, String> labels = null;
		try {
			Configuration configuration = new Configuration();
			// model is a matrix (wordId, labelId) => probability score
			NaiveBayesModel model = NaiveBayesModel.materialize(new Path(modelPath), configuration);
			StandardNaiveBayesClassifier classifier = new StandardNaiveBayesClassifier(model);

			// labels is a map label => classId
			labels = BayesUtils.readLabelIndex(configuration, new Path(labelIndexPath));
			Map<String, Integer> dictionary = readDictionnary(configuration, new Path(dictionaryPath));
			Map<Integer, Long> documentFrequency = readDocumentFrequency(configuration,
					new Path(documentFrequencyPath));

			// analyzer used to extract word from docs
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_34);

			int labelCount = labels.size();
			int documentCount = documentFrequency.get(-1).intValue();

			// Log.info("Number of labels: " + labelCount);
			/*
			 * for (int i = 0; i < labelCount; i++) Log.info("label " + i + ": " +
			 * labels.get(i));
			 */
			/*
			 * Log.info("Number of documents in training set: " + documentCount);
			 */
			// get string content to be classified

			Multiset<String> words = ConcurrentHashMultiset.create();

			// extract words from document
			TokenStream ts = analyzer.tokenStream("text", new StringReader(doc));
			CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
			ts.reset();
			int wordCount = 0;

			while (ts.incrementToken()) {
				if (termAtt.length() > 0) {
					String word = ts.getAttribute(CharTermAttribute.class).toString();
					Integer wordId = dictionary.get(word);
					// if the word is not in the dictionary, skip it
					if (wordId != null) {
						words.add(word);
						wordCount++;
					}
				}
			}

			// create vector wordId => weight using tfidf
			RandomAccessSparseVector vector = new RandomAccessSparseVector(10000);
			TFIDF tfidf = new TFIDF();
			for (Multiset.Entry<String> entry : words.entrySet()) {
				String word = entry.getElement();
				int count = entry.getCount();
				Integer wordId = dictionary.get(word);
				Long freq = documentFrequency.get(wordId);
				double tfIdfValue = tfidf.calculate(count, freq.intValue(), wordCount, documentCount);
				vector.setQuick(wordId, tfIdfValue);
			}

			// With the classifier, we get one score for each label
			// The label with the highest score is the one the tweet is more
			// likely to
			// be associated to
			Vector resultVector = classifier.classifyFull(vector);
			double bestScore = -Double.MAX_VALUE;

			// for(Element element:resultVector)
			for (int i = 0; i < resultVector.size(); i++) {
				Element element = resultVector.getElement(i);
				int categoryId = element.index();
				double score = element.get();
				if (score > bestScore) {
					bestScore = score;
					bestCategoryId = categoryId;
				}
				// LOG.info("scores " + labels.get(categoryId) + ": " + score);
			}
			// LOG.info("category => " + labels.get(bestCategoryId));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return labels.get(bestCategoryId);
	}

	/**
	 * @param conf
	 * @param documentFrequencyPath
	 * @return
	 */
	public static Map<Integer, Long> readDocumentFrequency(Configuration conf, Path documentFrequencyPath) {
		Map<Integer, Long> documentFrequency = new HashMap<Integer, Long>();
		for (Pair<IntWritable, LongWritable> pair : new SequenceFileIterable<IntWritable, LongWritable>(
				documentFrequencyPath, true, conf)) {
			documentFrequency.put(pair.getFirst().get(), pair.getSecond().get());
		}
		return documentFrequency;
	}

	public static Map<String, Integer> readDictionnary(Configuration conf, Path dictionnaryPath) {
		Map<String, Integer> dictionnary = new HashMap<String, Integer>();
		for (Pair<Text, IntWritable> pair : new SequenceFileIterable<Text, IntWritable>(dictionnaryPath, true, conf)) {
			dictionnary.put(pair.getFirst().toString(), pair.getSecond().get());
		}
		return dictionnary;
	}
	// ENDS HERE

	/**
	 * @param sourceLanguage
	 * @param targetLanguage
	 * @param searchElement
	 * @return
	 */
	public static String externalResponseForTranslation(String sourceLanguage, String targetLanguage,
			String searchElement) throws Exception {

		if (EXTERNAL_SERVICE_URL == null || EXTERNAL_SERVICE_URL.equals("")) {
			return "Service not intialized";
		}

		try {
			String url = EXTERNAL_SERVICE_URL + "/" + sourceLanguage + "/" + targetLanguage + "/" + searchElement.trim()
					+ "/statistical";
			System.out.println("url is  " + url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet http = new HttpGet(url);
			http.addHeader("accept", "application/json");
			HttpResponse serverResponseIITB = httpClient.execute(http);
			if (serverResponseIITB.getStatusLine().getStatusCode() == 200) {
				System.out.println("Response: " + serverResponseIITB);

				Scanner sc = new Scanner(new InputStreamReader(serverResponseIITB.getEntity().getContent()));
				String l = "";
				while (sc.hasNext()) {
					l += sc.next();
				}

				// String receivedResponse=g.toJson(l);
				String t = l.substring(l.indexOf("[") + 1, l.indexOf("]"));
				t = t.replaceAll("\"", "");
				sc.close();
				System.out.println("Retrived result from EXTERNAL Service for " + searchElement + ": " + t);
				return t;
			} else {
				http.abort();
				// return "Failed Response or Language Not Supported";
				throw new Exception("Failed Response or Language Not Supported");
			}

		} catch (Exception e) {
			throw e;
		}
		// return searchElement;

	}

	public static void main(String[] args) {
		ProcessingDao build = new ProcessingDao();
		try {
			System.out.println(build.getMapOfWordFromQuery(
					"Narendra Modi is in New York City right now flying in an Air India Plane", "en"));

			/*
			 * logger.log(Level.INFO, build.getCrossLingualMapFromSearchQuery("te",
			 * "కొత్త సంవత్సరం మొదటి రోజు")); logger.log(Level.INFO,
			 * build.getInterCrossLingualMapFromSearchQuery( "te", "pa",
			 * "కొత్త సంవత్సరం మొదటి రోజు"));
			 */
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}