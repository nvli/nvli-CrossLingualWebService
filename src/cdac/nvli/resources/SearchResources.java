package cdac.nvli.resources;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import cdac.nvli.bean.*;
import cdac.nvli.util.dao.*;

/**
 * @author lovey@cdacp
 * @category AAI group Crosslingual Web Service
 */
@Path("/webservice")
public class SearchResources {

	public final String[] resourceTypeCodes = { "ENEW", "GOVW", "WIKI", "VALUE" };
	private static ProcessingDao processingDao = null;
	static {
		processingDao = new ProcessingDao();
	}

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	public static Logger logger = Logger.getLogger("SearchResources");

	/**
	 * @param sourcelang
	 * @param searchElement
	 * @return Map<String, LinkedHashSet<Word>> where Word is a class object
	 *         containing translations
	 */
	/**
	 * @param lang
	 * @param searchElement
	 * @return
	 */
	@GET
	@Path("/crosslingual/{lang}/{searchElement}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Map<String, LinkedHashSet<Word>> getCrossLingual(@PathParam("lang") String lang,
			@PathParam("searchElement") String searchElement) {
		logger.log(Level.INFO, "path encountered: /crosslingual/{lang}/{searchElement}");
		Map<String, LinkedHashSet<Word>> map = new HashMap<>();
		if (searchElement.equals("")) {
			return map;
		}
		if (searchElement.equals("*.*")) {
			return map;
		}
		try {
			searchElement = searchElement.replaceAll("%20", " ").replaceAll("  ", " ").replaceAll("-", " ")
					.replaceAll(",", "").replaceAll("\\.", "").replaceAll("\\?", "").replaceAll("!", "");
			System.out.println("Searchelement : " + searchElement);
			// map = processingDao.getCrossLingualMapFromSearchQuery(lang, searchElement,
			// true);
			map = processingDao.getCrossLingualMapFromSearchQuery(lang, searchElement);

			logger.log(Level.INFO, "Map for direct crosslingual : " + map);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;

	}

	/**
	 * @param sourceLang
	 * @param targetLang
	 * @param searchElement
	 * @return Map<String, LinkedHashSet<Word>> where Word is a class object
	 *         containing translations
	 */
	@GET
	@Path("/crosslingual/{sourceLang}/{targetLang}/{searchElement}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Map<String, LinkedHashSet<Word>> getInterCrossLingual(@PathParam("sourceLang") String sourceLang,
			@PathParam("targetLang") String targetLang, @PathParam("searchElement") String searchElement) {
		logger.log(Level.INFO, "path encountered: /crosslingual/{sourceLang}/{targetLang}/{searchElement}");
		Map<String, LinkedHashSet<Word>> map = new HashMap<>();
		if (searchElement.equals("")) {
			return map;
		}
		if (searchElement.equals("*.*")) {
			return map;
		}
		try {
			searchElement = searchElement.replaceAll("%20", " ").replaceAll("  ", " ").replaceAll("-", " ")
					.replaceAll(",", "").replaceAll("\\.", "").replaceAll("\\?", "").replaceAll("!", "");
			map = processingDao.getInterCrossLingualMapFromSearchQuery(sourceLang, targetLang, searchElement);

			logger.log(Level.INFO, "Map for inter crosslingual is : " + map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * @param searchElement
	 * @return code of identified language e.g. en, hi, etc.
	 */
	@GET
	@Path("/detectlanguage/{searchElement}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String invokeLanguageDetection(@PathParam("searchElement") String searchElement) {
		logger.log(Level.INFO, "path encountered: /detectlanguage/{searchElement}");
		String identifiedLanguage = "";
		if (searchElement.equals("")) {
			return identifiedLanguage;
		}
		if (searchElement.equals("*.*")) {
			return identifiedLanguage;
		}
		try {
			searchElement = searchElement.replaceAll("%20", " ").replaceAll("  ", " ").replaceAll("-", " ")
					.replaceAll(",", "").replaceAll("\\.", "").replaceAll("\\?", "").replaceAll("!", "");
			identifiedLanguage = processingDao.getLanguage(searchElement);

			logger.log(Level.INFO, "Language identified for " + searchElement + " : " + identifiedLanguage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return identifiedLanguage;
	}

	@GET
	@Path("/addSuggestion/{sourceLanguage}/{targetLanguage}/{wordnetType}/{orignalElement}/{suggestionsString}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String addSuggestions(@PathParam("sourceLanguage") String sourceLanguage,
			@PathParam("targetLanguage") String targetLanguage, @PathParam("wordnetType") String wordnetType,
			@PathParam("orignalElement") String orignalElement,
			@PathParam("suggestionsString") String suggestionsString) {

		return "Failed";

	}

	public static void main(String[] args) {
		SearchResources searchResources = new SearchResources();
		String searchElement = "नरेंद्र मोदी";
		System.out.println("Language identified for " + searchElement + " is :"
				+ searchResources.invokeLanguageDetection(searchElement));
	}
}
