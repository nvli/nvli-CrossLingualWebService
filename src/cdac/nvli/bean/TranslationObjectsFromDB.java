package cdac.nvli.bean;


import java.util.LinkedHashSet;
import java.util.Map;

public class TranslationObjectsFromDB {

	String sourceLanguage;
	String targetLanguage;
	String characteristic;
	Map<String, LinkedHashSet<String>> translationObjects;

	/**
	 * @return the characteristic
	 */
	public String getCharacteristic() {
		return characteristic;
	}

	/**
	 * @param characteristic
	 *            the characteristic to set
	 */
	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	/**
	 * @return the sourceLanguage
	 */
	public String getSourceLanguage() {
		return sourceLanguage;
	}

	/**
	 * @param sourceLanguage
	 *            the sourceLanguage to set
	 */
	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	/**
	 * @return the targetLanguage
	 */
	public String getTargetLanguage() {
		return targetLanguage;
	}

	/**
	 * @param targetLanguage
	 *            the targetLanguage to set
	 */
	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	/**
	 * @return the translationObjects
	 */
	public Map<String, LinkedHashSet<String>> getTranslationObjects() {
		return translationObjects;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TranslationObjectsFromDB [sourceLanguage=" + sourceLanguage + ", targetLanguage=" + targetLanguage
				+ ", characteristic=" + characteristic + "]";
	}

	/**
	 * @param translationObjects
	 *            the translationObjects to set
	 */
	public void setTranslationObjects(Map<String, LinkedHashSet<String>> translationObjects) {
		this.translationObjects = translationObjects;
	}
}
