package cdac.nvli.bean;

import java.util.LinkedHashSet;

/**
 * @author Lovey Joshi
 *
 */
public class TranslationBean {

	String sourceLanguage;
	String targetLanguage;
	String sourceLanguageElement;
	LinkedHashSet<String> targetLanguageElements;


	/**
	 * @return the sourceLanguage
	 */
	public String getSourceLanguage() {
		return sourceLanguage;
	}

	/**
	 * @param sourceLanguage
	 *            the sourceLanguage to set
	 */
	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	/**
	 * @return the targetLanguage
	 */
	public String getTargetLanguage() {
		return targetLanguage;
	}

	/**
	 * @param targetLanguage
	 *            the targetLanguage to set
	 */
	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	/**
	 * @return the sourceLanguageElement
	 */
	public String getSourceLanguageElement() {
		return sourceLanguageElement;
	}

	/**
	 * @param sourceLanguageElement
	 *            the sourceLanguageElement to set
	 */
	public void setSourceLanguageElement(String sourceLanguageElement) {
		this.sourceLanguageElement = sourceLanguageElement;
	}

	/**
	 * @return the targetLanguageElements
	 */
	public LinkedHashSet<String> getTargetLanguageElements() {
		return targetLanguageElements;
	}

	/**
	 * @param targetLanguageElements
	 *            the targetLanguageElements to set
	 */
	public void setTargetLanguageElements(LinkedHashSet<String> targetLanguageElements) {
		this.targetLanguageElements = targetLanguageElements;
	}


}
