package cdac.nvli.bean;

import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Lovey Joshi
 *
 */
public class Word implements Comparable<Word>{
	String word;
	String language;
	String characteristic;
	String domain;
	String debug;
	Map<String, LinkedHashSet<String>> translations;

	/**
	 * @return the translations
	 */
	public Map<String, LinkedHashSet<String>> getTranslations() {
		return translations;
	}

	/**
	 * @param translations
	 *            the translations to set
	 */
	public void setTranslations(Map<String, LinkedHashSet<String>> translations) {
		this.translations = translations;
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	/**
	 * @return the word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * @param word
	 *            the word to set
	 */
	public void setWord(String word) {
		this.word = word;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the characteristic
	 */
	public String getCharacteristic() {
		return characteristic;
	}

	/**
	 * @param characteristic
	 *            the characteristic to set
	 */
	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain
	 *            the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @param word
	 * @param language
	 * @param characteristic
	 * @param domain
	 * @param debug
	 * @param translations
	 */
	public Word(String word, String language, String characteristic, String domain, String debug) {
		super();
		this.word = word;
		this.language = language;
		this.characteristic = characteristic;
		this.domain = domain;
		this.debug = debug;
		
	}

	public Word() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[word=" + word + ", language=" + language + ", characteristic=" + characteristic + ", domain=" + domain
				+ ", debug=" + debug + ", translations=" + translations + "]";
	}

	@Override
	public int compareTo(Word o) {
		// TODO Auto-generated method stub
		return o.getWord().compareTo(this.getWord());
	}

}
