-- CDAC Pune -- 
 June 12, 2017
-- -- --- -- --

NVLI Crosslingual Webservice Build 1.1 for HCDC Group by AAI Group

public functions over REST Api

@GET @Path("/crosslingual/{lang}/{searchElement}") : public Map<String, List<Word>>
@GET @Path("/crosslingual/{sourceLang}/{targetLang}/{searchElement}") : public Map<String, List<Word>>
@GET @Path("/detectlanguage/{searchElement}") : public String

User details: MetadataIntegratorUser : mit_user@123#

Use Rest Client for service check over http
Use cdac.nvli.client.ClientTester for testing Clientside output

Don't forget to put:
NE_DICTIONARY_LOCATION=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/ne_translation_dictionaries
TRANSLATION_DICTIONARY_LOCATION=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/translation_dictionaries
TRANSLITERATION_DICTIONARY_LOCATION=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/transliteration_lists
LANGUAGE_DETECT_PROFILE_DIRECTORY=/opt/NVLI_AllContent/NVLI_Hosting/Tomcat8/server_resources/ShailendraSir/server_resources/languageprofiles
at these locations. 